Pod::Spec.new do |s|
    s.name         = "Request"
    s.version      = "0.0.6"
    s.summary      = "A brief description of Request project."

    s.description  = <<-DESC
    An extended description of Request project.
    DESC

    s.homepage = "git clone https://bitbucket.org/ioasys/ioasys-networking-framework-ios"

    s.license = {
        :type => 'Copyright',
        :text => <<-LICENSE
            Copyright 2018
            Permission is granted to...
            LICENSE
    }

    s.author = {
        "ioasys" => "$(git config user.email)"
    }

    s.source = {
        :git => "https://bitbucket.org/ioasys/ioasys-networking-framework-ios.git",
        :tag => "#{s.version}"
    }

    s.vendored_frameworks = "Request.xcframework"
    s.swift_version = "5.4"

    s.ios.deployment_target = '10.0'
    s.tvos.deployment_target = '10.0'
    s.watchos.deployment_target = "4.0"
    s.macos.deployment_target = "10.14"
end
